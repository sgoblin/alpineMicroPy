FROM quay.io/justcontainers/base-alpine:latest
RUN apk update && apk add make gcc nano readline-dev libffi-dev musl-dev python-dev pkgconf git python3-dev util-linux
WORKDIR /tmp
RUN git clone https://github.com/micropython/micropython.git && cd micropython/unix && lscpu && make -j 4
CMD ["/bin/sh"]
